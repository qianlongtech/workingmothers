/*globals $, alert*/
var state = 0;
function frameLeave() {
    "use strict";
    $(".content > div").hide();
}

function frameLoad(x) {
    "use strict";
    frameLeave();
    var f = $("#section" + x);
    switch (x) {
    case 1:
        f.find(".content").css("background-image", "url(image/p0/@2xbeijing.png)");
        break;
    case 3:
        f.find(".content").css("background-color", "#fbf4fb");
        break;
    case 4:
        f.find(".content").css("background-image", "url(image/p3/@2xbaibj.png)");
        break;
    case 6:
        f.find(".content").css("background-image", "url(image/p5/bg.png)");
        break;
    case 7:
        f.find(".content").css("background-image", "url(image/p6/bg.png)");
        break;
    case 9:
        f.find(".content").css("background-image", "url(image/end.png)");
        break;
    }
    f.find("div").each(function (index) {
        $(this).delay(300 * index).fadeIn();
    });
}

$(document).ready(function () {
    "use strict";
    frameLoad(1);
    $("#fullpage").fullpage({
        afterLoad: function (anchorLink, index) {
            frameLoad(index);
        }
    });
});

$(".audio").click(function () {
    "use strict";
    if (state) {
        state = 0;
        $(".audio").css("background-image", "url(image/music.png)");
        document.getElementById("audio").play();
    } else {
        state = 1;
        $(".audio").css("background-image", "url(image/mute.png)");
        document.getElementById("audio").pause();
    }
});